/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

/**
 *
 * @author HP PAVILION
 */
public class Sintoma {
    private String name;
    private int prioridad;

    public Sintoma(String name, int prioridad) {
        this.name = name;
        this.prioridad = prioridad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    @Override
    public String toString() {
        return "name: " + name + ", prioridad: " + prioridad;
    }
    
    
}
