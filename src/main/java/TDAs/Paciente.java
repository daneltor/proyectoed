/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author USER
 */
public class Paciente {
    private String cedula;
    private String nombre;
    private String apellido;
    private int edad;
    private String genero;
    private Sintoma sintoma;

    public Paciente(String cedula,String nombre, String apellido, int edad, String genero, Sintoma sintoma) {
        this.cedula= cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
        this.sintoma = sintoma;
    }
    public Paciente(String cedula){
        this.cedula=cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Sintoma getSintoma() {
        return sintoma;
    }

    public void setSintoma(Sintoma sintoma) {
        this.sintoma = sintoma;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    public static List<Paciente> listaPacientes(){
        List<Paciente> lpacientes= new LinkedList<>();
        try ( BufferedReader bf = new BufferedReader(new FileReader("Paciente.txt"))) {
            bf.readLine();
            String linea;
            while ((linea = bf.readLine()) != null) {
                String p[] = linea.split(",");
                    lpacientes.add(new Paciente(p[0],p[1],p[2],Integer.valueOf(p[3]),p[4], new Sintoma(p[5],Integer.valueOf(p[6]))));
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return lpacientes;
    }
    
    public static void addPacie(Paciente p){
        try ( BufferedWriter bw = new BufferedWriter(new FileWriter("Medicos.txt",true))) {
            String cedula= p.getCedula();
            String name= p.getNombre();
            String apel= p.getApellido();
            String edad= String.valueOf(p.getEdad());
            String gen= p.getGenero();
            String siName= p.getSintoma().getName();
            String siPrio= String.valueOf(p.getSintoma().getPrioridad());
            bw.write(cedula + "," + name +"," + apel+","+edad+","+gen+","+siName+","+siPrio);
            bw.newLine();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Paciente{" + "cedula=" + cedula + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", genero=" + genero + ", sintoma=" + sintoma + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.cedula);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Paciente other = (Paciente) obj;
        if (!Objects.equals(this.cedula, other.cedula)) {
            return false;
        }
        return true;
    }

   
}
