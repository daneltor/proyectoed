/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author USER
 */
public class Medico {
    private String cedula;
    private String nombres;
    private String apellidos;
    private int edad;
    private String genero;
    private String especialidad;

    public Medico(String cedula, String nombres, String apellidos, int edad, String genero, String especialidad) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.genero = genero;
        this.especialidad = especialidad;
    }
    public Medico(String cedula){
        this.cedula = cedula;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String toString() {
        return "Medico{" + "cedula=" + cedula + ", nombres=" + nombres + ", apellidos=" + apellidos + ", edad=" + edad + ", genero=" + genero + ", especialidad=" + especialidad + '}';
    }
    
    public static LinkedList<Medico> listaMedicos(){
        LinkedList<Medico> lmedicos = new LinkedList<>();
        try ( BufferedReader bf = new BufferedReader(new FileReader("src/recursos/datos del medico.txt"))) {
            String linea="";
            bf.readLine();
            while ((linea = bf.readLine()) != null) {
                String p[] = linea.split(",");
                    lmedicos.add(new Medico(p[0],p[1],p[2],Integer.parseInt(p[3]),p[4],p[5]));
                
            }
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return lmedicos;                
    }
    
    public static void addmedic(Medico m){
        try ( BufferedWriter bw = new BufferedWriter(new FileWriter("src/recursos/Medicos.txt",true))) {
            String cedula= m.getCedula();
            String name= m.getNombres();
            String apel= m.getApellidos();
            String edad= String.valueOf(m.getEdad());
            String gen= m.getGenero();
            String espe= m.getEspecialidad();
             bw.write(cedula + "," + name +"," + apel+","+edad+","+gen+","+espe);
             bw.newLine();
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
