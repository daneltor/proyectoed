/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;

import TDAs.Medico;
import java.util.Objects;

/**
 *
 * @author USER
 */
public class Puesto {
    private String idPuesto;
    private boolean ocupado;
    private Medico medico;
    public Puesto(String idPuesto, Medico medico) {
        this.idPuesto = idPuesto;
        this.medico = medico;
        ocupado = false;
    }

    public Puesto(String idPuesto) {
        this.idPuesto = idPuesto;
        ocupado = false;
    }
    
    public String getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(String idPuesto) {
        this.idPuesto = idPuesto;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setOcupado(boolean ocupado) {
        this.ocupado = ocupado;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    @Override
    public String toString() {
        return "Puesto{" + "idPuesto=" + idPuesto + ", ocupado=" + ocupado + ", medico=" + medico + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.idPuesto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Puesto other = (Puesto) obj;
        if (!Objects.equals(this.idPuesto, other.idPuesto)) {
            return false;
        }
        return true;
    }
    
    public void liberarPuesto(){
        
        
    }

}
