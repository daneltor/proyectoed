/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TDAs;
 
/**
 *
 * @author USER
 */
public class Turno {
    private Medico medico;
    private Paciente paciente;
    private Puesto puesto;
    private String idTurno;

    public Turno(Medico medico, Paciente paciente, Puesto puesto, String id) {
        this.medico = medico;
        this.paciente = paciente;
        this.puesto = puesto;
        idTurno=id;
    }

    public String getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(String idTurno) {
        this.idTurno = idTurno;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "Turno{" + "medico=" + medico + ", paciente=" + paciente + ", idTurno=" + idTurno + '}';
    }
   
}