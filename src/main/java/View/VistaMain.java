/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author danie
 */
public class VistaMain {
    private BorderPane root;
    private VBox botones;


    public VistaMain(){
        root= new BorderPane();
        seccionBotones();
    }


    /**
     *
     */
    public void seccionBotones(){
        botones= new VBox();
        Button exp= new Button("Explorar");
        Button plan= new Button("Planificar Rutas");
        Button video= new Button("Videos");
        Button sal= new Button("Salir");
        botones.getChildren().addAll(exp,plan,video,sal);
        root.setCenter(botones);
        botones.setSpacing(10);
        botones.setAlignment(Pos.CENTER);

        exp.setOnAction((e)->{
            Stage s= new Stage();
            Scene sc= new Scene(new VistaExplorar().getRaiz());
            s.setScene(sc);
            s.show();
        });

        sal.setOnAction((e) -> {
            System.exit(0);
        }
        );

        video.setOnAction((ev)->{
            Stage s= new Stage();
            Scene sc= new Scene(new VistaTV().getRaiz(),700,500);
            s.setScene(sc);
            s.show();
        });

        plan.setOnAction((e)->{
           Stage s= new Stage();
           Scene sc=new Scene(new VistaPlan().getRaiz(),700,500);
           s.setScene(sc);
           s.show();
        });
    }


    public BorderPane getRoot() {
        return root;
    }
    
}
